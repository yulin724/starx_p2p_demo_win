﻿#include <windows.h>
#include <stdio.h>
#include <direct.h>

#define WIN32DLL 1

#ifdef WIN32DLL
#include "STARX/STARX_API.h"
#pragma comment(lib,"ws2_32")
#endif

#define CHANNEL_DATA	1
#define MAX_SIZE_BUF	64*1024

INT32 main(INT32 argc, CHAR **argv) {

	if (argc != 3) {
		printf("Usage: TransferFileDemo_Client DeviceID FilePath\n");
		printf("Example: TransferFileDemo_Client XXX-000000-ZZZZZ ./README\n");
		return 0;
	}

	UINT32 APIVersion = STARX_GetAPIVersion();
	printf("STARX_API Version: %d.%d.%d.%d\n", (APIVersion & 0xFF000000) >> 24,
			(APIVersion & 0x00FF0000) >> 16, (APIVersion & 0x0000FF00) >> 8,
			(APIVersion & 0x000000FF) >> 0);

	INT32 ret;
	ret =
			STARX_Initialize(
					(CHAR*) "EFGBFFBJKFJOGCJNFHHCFHEMGENHHBMHHLFGBKDFAMJLLDKHDHACDEPBGCLAIALDADMPKDDIODMEBOCNJLNDJJ");

	st_STARX_NetInfo NetInfo;
	ret = STARX_NetworkDetect(&NetInfo, 0);

	printf("-------------- NetInfo: -------------------\n");
	printf("Internet Reachable     : %s\n",
			(NetInfo.bFlagInternet == 1) ? "YES" : "NO");
	printf("P2P Server IP resolved : %s\n",
			(NetInfo.bFlagHostResolved == 1) ? "YES" : "NO");
	printf("P2P Server Hello Ack   : %s\n",
			(NetInfo.bFlagServerHello == 1) ? "YES" : "NO");
	printf("Local NAT Type         :");
	STARX_Share_Bandwidth(1);
	switch (NetInfo.NAT_Type) {
	case 0:
		printf(" Unknow\n");
		break;
	case 1:
		printf(" IP-Restricted Cone\n");
		break;
	case 2:
		printf(" Port-Restricted Cone\n");
		break;
	case 3:
		printf(" Symmetric\n");
		break;
	}
	printf("My Wan IP : %s\n", NetInfo.MyWanIP);
	printf("My Lan IP : %s\n", NetInfo.MyLanIP);

//关键部分

	INT32 SessionHandle = 0;
	SessionHandle = STARX_Connect(argv[1], 1, 0);
	if (SessionHandle >= 0) {

		st_STARX_Session Sinfo;
		if (STARX_Check(SessionHandle, &Sinfo) == ERROR_STARX_SUCCESSFUL) {
			printf("\n-------Session(%d) Ready %s------------------\n",
					SessionHandle, (Sinfo.bMode == 0) ? "P2P" : "RLY");
			printf("Socket FD: %d\n", Sinfo.Skt);
			printf("Remote Address : %s:%d\n",
					inet_ntoa(Sinfo.RemoteAddr.sin_addr),
					ntohs(Sinfo.RemoteAddr.sin_port));

			FILE *fp = fopen(argv[2], "wb");

			CHAR *pBuf = (CHAR *) malloc(MAX_SIZE_BUF);

			ULONG total_recv_bytes = 0;

			while (1) {
				INT32 nRet = 0, rsize = 0;
				//检查P2PBuffer中可能读取的字节数
				nRet = STARX_Check_Buffer(SessionHandle, CHANNEL_DATA, NULL,
						(UINT32 *) &rsize);
				if (nRet == ERROR_STARX_SESSION_CLOSED_TIMEOUT) {
					printf("TransferFileDemo_Device, Session TimeOUT!!\n");
					break;
				} else if (nRet == ERROR_STARX_SESSION_CLOSED_REMOTE) {
					printf("TransferFileDemo_Device, Session Remote Close!!\n");
					break;
				} else if (nRet == ERROR_STARX_INVALID_SESSION_HANDLE) {
					printf(
							"TransferFileDemo_Device, invalid session handle!!\n");
					break;
				}
				if (rsize > MAX_SIZE_BUF) {
					rsize = MAX_SIZE_BUF;
				}

				if (rsize > 0) {
					//从P2P Buffer中都去指定数量的字节
					nRet = STARX_Read(SessionHandle, CHANNEL_DATA, pBuf, &rsize,
							10);

					total_recv_bytes += rsize;
					printf("已经收到 %d 字\n", total_recv_bytes);
					//printf("]\n\033[F\033[J"); //clear current Line

					fwrite(pBuf, rsize, 1, fp);
				}
				Sleep(20);
			}
			printf("\nSessionHandle is Closed.\n");
			fclose(fp);

		} else if (SessionHandle == ERROR_STARX_INVALID_ID) {
			printf("Invalid DID \n");
		}
	}

//--关键部分

	ret = STARX_DeInitialize();
	printf("....Job Done!! press any key to exit\n");
	getchar();

	return 0;
}
