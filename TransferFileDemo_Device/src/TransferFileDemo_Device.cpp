﻿#include <windows.h>
#include <stdio.h>
#include <direct.h>

#define WIN32DLL 1

#ifdef WIN32DLL
#include "STARX/STARX_API.h"
#pragma comment(lib,"ws2_32")
#endif

#define CHANNEL_DATA	1
#define MAX_SIZE_BUF	64*1024

UINT32 myGetTickCount() {return GetTickCount();}

INT32 main(INT32 argc, CHAR **argv) {

	if (argc != 3) {
		printf("Usage: TransferFileDemo_Device DeviceID FilePath\n");
		printf("Example: TransferFileDemo_Device XXX-000000-ZZZZZ ./README\n");
		return 0;
	}

	UINT32 APIVersion = STARX_GetAPIVersion();
	printf("STARX_API Version: %d.%d.%d.%d\n", (APIVersion & 0xFF000000) >> 24,
			(APIVersion & 0x00FF0000) >> 16, (APIVersion & 0x0000FF00) >> 8,
			(APIVersion & 0x000000FF) >> 0);

	INT32 ret;
	ret =
			STARX_Initialize(
					(CHAR*) "EFGBFFBJKFJOGCJNFHHCFHEMGENHHBMHHLFGBKDFAMJLLDKHDHACDEPBGCLAIALDADMPKDDIODMEBOCNJLNDJJ");

	st_STARX_NetInfo NetInfo;
	ret = STARX_NetworkDetect(&NetInfo, 0);

	printf("-------------- NetInfo: -------------------\n");
	printf("Internet Reachable     : %s\n",
			(NetInfo.bFlagInternet == 1) ? "YES" : "NO");
	printf("P2P Server IP resolved : %s\n",
			(NetInfo.bFlagHostResolved == 1) ? "YES" : "NO");
	printf("P2P Server Hello Ack   : %s\n",
			(NetInfo.bFlagServerHello == 1) ? "YES" : "NO");
	printf("Local NAT Type         :");
	STARX_Share_Bandwidth(1);
	switch (NetInfo.NAT_Type) {
	case 0:
		printf(" Unknow\n");
		break;
	case 1:
		printf(" IP-Restricted Cone\n");
		break;
	case 2:
		printf(" Port-Restricted Cone\n");
		break;
	case 3:
		printf(" Symmetric\n");
		break;
	}
	printf("My Wan IP : %s\n", NetInfo.MyWanIP);
	printf("My Lan IP : %s\n", NetInfo.MyLanIP);

//关键部分

	while (1) {
		INT32 SessionHandle = 0;
		printf("Device: STARX_Listen\n");
		SessionHandle = STARX_Listen(argv[1], 600, 0, 1);
		if (SessionHandle >= 0) {

			st_STARX_Session Sinfo;
			if (STARX_Check(SessionHandle, &Sinfo) == ERROR_STARX_SUCCESSFUL) {
				printf("\n-------Session(%d) Ready %s------------------\n",
						SessionHandle, (Sinfo.bMode == 0) ? "P2P" : "RLY");
				printf("Socket FD: %d\n", Sinfo.Skt);
				printf("Remote Address : %s:%d\n",
						inet_ntoa(Sinfo.RemoteAddr.sin_addr),
						ntohs(Sinfo.RemoteAddr.sin_port));

				printf("client connected\n");

				FILE *fp = fopen(argv[2], "rb");
				fseek(fp, 0, SEEK_END);
				long len = ftell(fp);
				fseek(fp, 0, SEEK_SET);

				long offset = 0;

				unsigned long start_tick = myGetTickCount();
				unsigned long total_send_bytes = 0;

				CHAR *pBuf = (CHAR *) malloc(MAX_SIZE_BUF);
				while (1) {
					size_t bufsize = fread(pBuf, 1, MAX_SIZE_BUF, fp);

					if (bufsize == 0) {
						printf("输入文件读取完成\n");
						break;
					}

					INT32 nRet = 0, wsize = 0;

					while (1) {
						nRet = STARX_Check_Buffer(SessionHandle, CHANNEL_DATA,
								(UINT32 *) &wsize,
								NULL);
						if (nRet == ERROR_STARX_SESSION_CLOSED_TIMEOUT) {
							printf(
									"TransferFileDemo_Device, Session TimeOUT!!\n");
							break;
						} else if (nRet == ERROR_STARX_SESSION_CLOSED_REMOTE) {
							printf(
									"TransferFileDemo_Device, Session Remote Close!!\n");
							break;
						} else if (nRet == ERROR_STARX_INVALID_SESSION_HANDLE) {
							printf(
									"TransferFileDemo_Device, invalid session handle!!\n");
							break;
						}
						if (wsize > 65536) { // p2p buffer还有很多数据要写，所以，等待数据写出完成
//							printf("等待p2p write buffer有空闲");
//							printf("]\n\033[F\033[J"); //clear current line
							Sleep(20);
							continue;
						} else {
							break;
						}
					}
					nRet = STARX_Write(SessionHandle, CHANNEL_DATA, pBuf,
							bufsize);

					ULONG cur_tick = myGetTickCount();
					ULONG duration = cur_tick - start_tick;
					total_send_bytes += nRet;
					if (duration == 0) {
						printf("成功写入 %d 字节\n", nRet);
					} else {
						printf("%f%, 成功写入 %d 字节, 速度：%d KB/s\n",
								total_send_bytes * 100.0 / len, total_send_bytes,
								total_send_bytes / duration);
					}
					//printf("]\n\033[F\033[J"); //clear current line

					Sleep(20);
				}
				printf("All data is completed.\n");
				fclose(fp);

				printf("等待p2p write buffer中的数据写完\n");
				while (1) {
					INT32 wsize = 0;
					STARX_Check_Buffer(SessionHandle, CHANNEL_DATA,
							(UINT32 *) &wsize,
							NULL);
					if (wsize == 0) {
						printf("    p2p write buffer中的数据写完\n");
						break;
					} else {
						Sleep(20);
					}
				}

				printf("关闭SessionHandle\n");
				STARX_Close(SessionHandle);

			} else if (SessionHandle == ERROR_STARX_INVALID_ID) {
				printf("Invalid DID \n");
				break;
			}
		}
	}

//--关键部分

	ret = STARX_DeInitialize();
	printf("....Job Done!! press any key to exit\n");
	getchar();

	return 0;
}
